using System.Collections.ObjectModel;
using System.Net.Http;
using System.Net.Http.Json;
using Mopups.Services;

namespace MobileProject.Views;

public partial class JoinMember : ContentPage
{

    /*public JoinMember()
	{
		InitializeComponent();
    }*/


    /*private void OnCounterClicked(object sender, EventArgs e)
    {
        App.Current.MainPage = new NavigationPage(new JoinMember());

        SemanticScreenReader.Announce(CounterBtn.Text);
    }*/

    private readonly HttpClient httpClient = new();

    public bool IsRefreshing { get; set; }
    public ObservableCollection<DetailProject> DetailProjects { get; set; } = new();
    public Command RefreshCommand { get; set; }
    public Monkey SelectedDetailProject { get; set; }

    


    public JoinMember()
    {
        RefreshCommand = new Command(async () =>
        {
            // Simulate delay
            await Task.Delay(2000);

            await LoadDetailProjects();

            IsRefreshing = false;
            OnPropertyChanged(nameof(IsRefreshing));
        });

        BindingContext = this;

        InitializeComponent();
    }

 

protected async override void OnNavigatedTo(NavigatedToEventArgs args)
    {
        base.OnNavigatedTo(args);

        await LoadDetailProjects();
    }

    private void Button_Clicked(object sender, EventArgs e)
    {
        DetailProjects.Clear();
    }

    private async Task LoadDetailProjects()
    {
        var detailprojects = await httpClient.GetFromJsonAsync<DetailProject[]>("https://gitlab.com/DimaKeda/MAUI/-/raw/master/projectdataPDetail.json?ref_type=heads");

        DetailProjects.Clear();

        foreach (DetailProject detailproject in DetailProjects)
        {
            DetailProjects.Add(detailproject);
        }
    }

    private void ConfirmationButton_Clicked(object sender, EventArgs e)
    {
        MopupService.Instance.PushAsync(new DetailJobConfirmation());
    }

   /* private List<Monkey> SelectedEngineOptions(long vehicleId, int? styleId, List<VehicleOptionalEquipment> optionalEquips)
    {

    }*/

}