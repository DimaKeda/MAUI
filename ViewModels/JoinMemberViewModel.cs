﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Net.Http.Json;

namespace MobileProject.ViewModels
{
    public class JoinMemberViewModel
    {
        private readonly HttpClient httpClient = new();

        
        public ObservableCollection<Monkey> Monkeys { get; set; } = new();
        public Monkey SelectedMonkey { get; set; }
        /* static Random random = new();
         public ObservableCollection<Student> Items { get; } = new();
         public JoinMemberViewModel()
         {
             for (int i = 0; i < 10; i++)
             {
                 Items.Add(new Student
                 {
                     Id = i,
                     Name = "Person " + i,
                     Age = random.Next(14, 85),
                 });
             }
         }
         public class Student
         {
             public int Id { get; set; }
             public string Name { get; set; }
             public int Age { get; set; }
         }*/
        /* public ObservableCollection<object> ContactsInfo { get; set; }

         public JoinMemberViewModel()
         {
             GenerateInfo();
         }

         private void GenerateInfo()
         {
             Random random = new Random();

             ContactsInfo = new ObservableCollection<object>();

             DataTable dt = new DataTable("Contacts");
             dt.Columns.Add("ContactID", typeof(Int32));
             dt.Columns.Add("ContactName", typeof(string));
             dt.Columns.Add("ContactType", typeof(string));
             dt.Columns.Add("ContactNumber", typeof(string));

             for (int i = 0; i < CustomerNames.Count(); i++)
             {
                 dt.Rows.Add(i + 1, CustomerNames[i], contactType[random.Next(0, 5)], random.Next(100, 400).ToString() + "-" + random.Next(500, 800).ToString() + "-" + random.Next(1000, 2000).ToString());
             }

             for (int i = 0; i < dt.Rows.Count; i++)
             {
                 ContactsInfo.Add(dt.Rows[i]);
             }
         }*/

        public async Task LoadMonkeys()
        {
            var monkeys = await httpClient.GetFromJsonAsync<Monkey[]>("https://montemagno.com/monkeys.json");

            Monkeys.Clear();

            foreach (Monkey monkey in monkeys)
            {
                Monkeys.Add(monkey);
            }
        }


        private void Button_Clicked(object sender, EventArgs e)
        {
            Monkeys.Clear();
        }

       
    }
}
